﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HM20_OrElAmi
{
    public partial class Form1 : Form
    {
        PictureBox pic;
        NetworkStream clientStream;
        int cardValue = -1;

        public Form1()
        {
            InitializeComponent();
        }

        private void GenerateCards()
        {
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(label1.Location.X, pictureBox1.Location.Y + pictureBox1.Size.Height + 80);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::HM20_OrElAmi.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(91, 118);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    if (cardValue == -1)
                    {
                        if (pic != null)
                        {
                            Invoke((MethodInvoker)delegate
                            {
                                pictureBox1.Image = Properties.Resources.card_back_blue;
                                pictureBox1.Refresh();
                                pic.Image = Properties.Resources.card_back_red;
                                pic.Refresh();
                            });
                        }
                        pic = currentPic;
                        string message = "1";
                        int value = (new Random()).Next(13) + 1;
                        if (value < 10) message += "0";
                        message += value.ToString();
                        switch ((new Random()).Next(4))
                        {
                            case 0: message += "H"; break;
                            case 1: message += "C"; break;
                            case 2: message += "S"; break;
                            case 3: message += "D"; break;
                        }
                        Invoke((MethodInvoker)delegate { currentPic.Image = Card(int.Parse(message.Substring(1, 2)), message[3]); currentPic.Refresh(); });
                        cardValue = value;
                        byte[] buffer = new ASCIIEncoding().GetBytes(message);
                        clientStream.Write(buffer, 0, 4);
                        clientStream.Flush();
                    }
                };

                // add the picture object to the form (otherwise it won't be seen)
                Invoke((MethodInvoker) delegate { this.Controls.Add(currentPic); });

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
                if (nextLocation.X > this.Size.Width)
                {
                    // move to next line below
                    nextLocation.X = label1.Location.X;
                    nextLocation.Y += currentPic.Size.Height + 10;
                }
            }
        }

        private Bitmap Card(int card, char type)
        {
            string cardName = "";
            if (card > 1 && card < 11) cardName = "_" + card.ToString();
            else switch (card)
                {
                    case 1: cardName = "ace"; break;
                    case 11: cardName = "jack"; break;
                    case 12: cardName = "queen"; break;
                    case 13: cardName = "king"; break;
                }
            cardName += "_of_";
            switch(type)
            {
                case 'H': cardName += "hearts"; break;
                case 'C': cardName += "clubs"; break;
                case 'S': cardName += "spades"; break;
                case 'D': cardName += "diamonds"; break;
            }

            return (Bitmap)global::HM20_OrElAmi.Properties.Resources.ResourceManager.GetObject(cardName);
        }

        private void communication()
        {
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();
                while (true)
                {
                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);
                    if (input == "0000")
                    {
                        Invoke((MethodInvoker)delegate { forfeit.Enabled = true; });
                    }
                    if (input[0] == '1')
                    {
                        while (cardValue == -1) ;
                        Invoke((MethodInvoker)delegate {
                            pictureBox1.Image = Card(int.Parse(input.Substring(1, 2)), input[3]);
                            pictureBox1.Refresh();
                            if (cardValue > int.Parse(input.Substring(1, 2)))
                                label1.Text = label1.Text.Substring(0, 12) + (int.Parse(label1.Text.Substring(12)) + 1).ToString();
                            if (cardValue < int.Parse(input.Substring(1, 2)))
                                label2.Text = label2.Text.Substring(0, 18) + (int.Parse(label2.Text.Substring(18)) + 1).ToString();
                            cardValue = -1;
                        });
                    }
                    if (input == "2000") Application.Exit();
                    GenerateCards();
                }
            }
            catch(Exception)
            {
                Application.ExitThread();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            forfeit.Enabled = false;
            Thread t = new Thread(communication);
            t.Start();
        }

        private void forfeit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            MessageBox.Show(label1.Text + "\n" + label2.Text);
        }
    }
}
